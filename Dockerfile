From python:3.5-alpine
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.pip /tmp/
RUN set -ex \
    && apk add --no-cache --virtual .build-deps \
        alpine-sdk \
        wget \
        git \
        libffi-dev \
        libxml2-dev \
        libxslt-dev \
        libjpeg-turbo \
        libjpeg-turbo-dev \
        zlib-dev \
        libpng-dev \
        fontconfig \
        ttf-dejavu \
        ttf-droid-nonlatin \
        ttf-liberation \
        poppler-utils \
        linux-headers \
        postgresql-dev \
    && pip install -U pip \
    && pip install --no-cache-dir -U -r /tmp/requirements.pip \
    && runDeps="$( \
            scanelf --needed --nobanner --recursive /venv \
                    | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
                    | sort -u \
                    | xargs -r apk info --installed \
                    | sort -u \
    )" \
    && apk add --virtual .python-rundeps $runDeps \
    && apk del .build-deps
ADD . /code/
