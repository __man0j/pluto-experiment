from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^portfolio/$', views.index, name='index'),
    url(r'^$', views.about, name='about'),
    url(r'^contact/$', views.contact, name='contact'),
]
