from django.db import models
from django.utils.text import slugify

def upload_location(instance,filename):
    PostModel= instance.__class__
    new_id = PostModel.objects.order_by("id").last().id +1
    return "%s/%s" %(new_id,filename)

class Projects(models.Model):
    title = models.CharField(max_length=120)
    slug = models.SlugField(unique=True)
    title_image = models.ImageField(upload_to=upload_location,
            null=True,
            blank=True,
            verbose_name = "Title Image"
            )
    description = models.TextField(blank=True)
    language = models.CharField(max_length = 16)

    def __str__(self):
        return self.title

class Images(models.Model):
    project = models.ForeignKey('Projects', default=None)
    image = models.ImageField(upload_to=upload_location,
            null=True,
            blank=True,
            verbose_name='Images',
            )

def create_slug(instance, new_slug = None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Post.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_slug(instance, new_slug=new_slug)
    return slug


