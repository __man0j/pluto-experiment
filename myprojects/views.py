from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives

from .models import (
        Images,
        Projects,
    )
# Create your views here.


def index(request):
    return render(request,'index.html')

def about(request):
    return render(request,'about.html')

def contact(request):
    if request.method== 'POST':
        first_name=request.POST['name']
        last_name=request.POST['surname']
        num=request.POST['num']
        mailaddress=request.POST['email']
        message=request.POST['message']
        name = first_name+' '+last_name

        if name=='' or num=='' or name==' ' or num==' ':
            messages.warning(request,'Please recheck form again and try !!!')


        subject,seller,to='Message to mane','mail@sarober.com','manesarober@gmail.com'
        html_content='<h2>New Message to Mane:</h2> <p>Name: '+name+'<br>'+'phone num:'+num+'<br>'+ 'mailaddress:'+mailaddress+'<br>'+ 'message:'+message+'</p>'

        email=EmailMultiAlternatives(subject,'''Message to mane''',seller,['chapagainmanoj35@gmail.com','bb.bindash35@gmail.com'])
        email.attach_alternative(html_content,'text/html')
        email.send()

        messages.success(request,'Thanks For Your Message')
    return render(request,'contact.html')
